#!/usr/bin/env bash

USER=puma

# Must be run as root
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root"
   exit 1
fi

# Copy the systemd config for puma
cp config/puma.service /etc/systemd/system/

# Create the puma user if it doesn't exist
if getent passwd "${USER}" > /dev/null 2>&1; then
    adduser --system --shell /bin/bash --home /var/www/switch puma
fi

# Copy rhe rest of the files
cp -R ./* /var/www/switch/

# Install ruby and the gems needed
apt install ruby ruby-dev -y
gem install bundler
bundle install

# Install a puma binstub
pushd  || exit
/var/www/switch
bundle binstubs puma --path ./sbin

chown puma: -R /var/www/switch

if [[ ! -f "/etc/systemd/system/puma.service" ]]; then
    systemctl enable puma.service
fi

systemctl daemon-reload

systemctl restart puma

# install nginx if not currently installed
if [[ ! $(nginx) ]]; then
  apt install nginx-full -y
fi

# Copy the nginx conf files.
cp -R config/nginx.conf /etc/nginx/conf.d/switch.conf
systemctl enable nginx
systemctl start nginx

# Set the path for the ovs certs
ovs-vsctl set-ssl /var/www/certs/ovs_key.pem /var/www/certs/ovs_cert.pem /var/www/certs/cacert.pem
