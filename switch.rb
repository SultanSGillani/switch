# frozen_string_literal: true

require 'sinatra/base'
require 'openssl'
require 'json'

class Switch < Sinatra::Base
  set :server, 'puma'
  set :show_exceptions, false
  set :app_file, __FILE__
  set :root, File.dirname(__FILE__)
  set :logging, true
  set :environment, :production
  set :run, false

  EXTENSION_TYPES = {
    js: 'text/javascript',
    css: 'text/css',
    svg: 'image/svg+xml',
    png: 'image/png',
    json: 'application/json',
    csv_file: 'application/csv',
    csv: 'text/plain',
  }.freeze

  helpers do
    # stolen from routes.rb

    def json_resp(content, status_code = 200)
      content_type EXTENSION_TYPES[:json]
      content = content.as_json if content.respond_to? :as_json
      [status_code, content ? JSON.pretty_generate(content) : 'null']
    end
  end

  post '/upload' do
    # validate the ovs cert a file was uploaded
    unless params.include?('file')
      json_resp({ error: 'No file provided' }, 400)
    end

    # get our file information and validate that we've got a known file extension
    filename = params[:file][:filename]
    file_ext = File.extname(filename)
    unless %w(.crt .cer .pem .pfx .p12 .txt).include?(file_ext)
      json_resp({ error: "Unsupported file extension \"#{file_ext}\"" }, 400)
    end

    file = params[:file][:tempfile]

    path = "/var/www/switch/certs/#{filename}"
    File.open(path, 'wb') do |f|
      f.write(file.read)
    end
    json_resp({}, 201)
  end
end

if __FILE__ == $PROGRAM_NAME
  Switch.run!
end
